<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project}}`.
 */
class m210504_162651_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Связь с пользователем'),
            'name'=> $this->string()->notNull()->comment('Название'),
            'cost'=> $this->decimal(8,2)->comment('стоимость'),
            'start_date'=>$this->date()->notNull()->comment('дата начало'),
            'end_date'=>$this->date()->comment('дата сдачи'),
            'is_deleted'=>$this->boolean(),
        ], $tableOptions);

        $this->addForeignKey('project-user_id-user-id','project','user_id','user','id');
    }

   /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project}}');
    }
}
