<?php

use app\models\User;
use yii\base\BaseObject;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210504_161740_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique()->comment('логин'),
            'email' => $this->string()->notNull()->unique(),
            'fio' => $this->string()->notNull()->comment('фио'),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull()->comment('хеш пароля'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
        ], $tableOptions);
        $model = new User();
            $model->username= 'admin';
            $model->email = 'admin@admin.com';
            $model->fio= 'Админ Админов';
            $model->setPassword('admin');
            $model->generateAuthKey();
            $model->save();
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
