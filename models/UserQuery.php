<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @return UserQuery
     */
    public function active()
    {
        return $this->andWhere([User::tableName() . '.status' => User::STATUS_ACTIVE]);
    }

    /**
     * @param false $all
     */
    public function getActiveList($all = false)
    {
        $list = ArrayHelper::map(User::find()->andWhere([User::tableName() . '.status' => User::STATUS_ACTIVE])->all(),
            'id', 'username');
        if ($all) {
            $list = ArrayHelper::merge([null => 'Все'], $list);
        }
        return $list;
    }

    /**
     * @param false $all
     */
    public function getAllList($all = false)
    {
        $list = ArrayHelper::map(User::find()->all(), 'id', 'username');
        if ($all) {
            $list = ArrayHelper::merge([null => 'Все'], $list);
        }
        return $list;
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
