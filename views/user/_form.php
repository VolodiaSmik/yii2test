<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord) {
        echo $form->field($model, 'password_hash')->textInput(['maxlength' => true])->label('Пароль');
    } else {
        echo $form->field($model, 'password')->textInput(['maxlength' => true,'value'=>''])->label('Сменить пароль');
    }
    ?>

    <?= $form->field($model, 'status')->checkbox(['value' => 10,
        'uncheck' => 0]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
